package com.weblogism.camel.sandbox;

import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@Data
@CsvRecord(separator="\t")
public class TreeRecord {
    public final static String[] COUNTIES = {
        "Carlow",
        "Cavan",
        "Clare",
        "Cork",
        "Donegal",
        "Dublin",
        "Galway",
        "Kerry",
        "Kildare",
        "Kilkenny",
        "Laois",
        "Leitrim",
        "Limerick",
        "Longford",
        "Louth",
        "Mayo",
        "Meath",
        "Monaghan",
        "Offaly",
        "Roscommon",
        "Sligo",
        "Tipperary",
        "Waterford",
        "Westmeath",
        "Wicklow"
    };

    @DataField(pos = 1)
    private String recordKey;

    @DataField(pos = 2)
    private String surveyKey;

    @DataField(pos = 3)
    private String sampleKey;

    @DataField(pos = 4)
    private String startDate;

    @DataField(pos = 5)
    private String endDate;

    @DataField(pos = 6)
    private String dateType;

    @DataField(pos = 7)
    private String date;

    @DataField(pos = 8)
    private String taxonVersionKey;

    @DataField(pos = 9)
    private String taxonName;

    @DataField(pos = 10)
    private String gridReference;

    @DataField(pos = 11)
    private String east;

    @DataField(pos = 12)
    private String north;

    @DataField(pos = 13)
    private String projection;

    @DataField(pos = 14)
    private String precision;

    @DataField(pos = 15)
    private String siteKey;

    @DataField(pos = 16)
    private String siteName;

    @DataField(pos = 17)
    private String recorder;

    @DataField(pos = 18)
    private String determiner;

    @DataField(pos = 19)
    private String zeroAbundance;

    @DataField(pos = 20)
    private String underValidation;

    @DataField(pos = 21)
    private String imagePath;

    @DataField(pos = 22)
    private String heritageType;

    @DataField(pos = 23)
    private String categoryOfTree;

    @DataField(pos = 24)
    private String treeForm;

    @DataField(pos = 25)
    private String condition;

    @DataField(pos = 26)
    private String ageRange;

    @DataField(pos = 27)
    private String evidenceOf;

    @DataField(pos = 28)
    private String access;

    @DataField(pos = 29)
    private String variety;


    public String getCounty() {
        for (String county: COUNTIES) {
            if (siteName.toLowerCase().contains(county.toLowerCase())) {
                return county;
            }
        }

        return "Unknown";
    }
}
