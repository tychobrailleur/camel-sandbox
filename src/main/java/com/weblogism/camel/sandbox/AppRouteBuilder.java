package com.weblogism.camel.sandbox;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;

import java.util.Map;

public class AppRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        from("file:///home/sebastien/dev/experiments/camel-sandbox/src/main/resources/?noop=true")
            .tracing()
            .split(body().tokenize("\n"))
            .streaming()
            .parallelProcessing() // Parallel processing so that each split message is processed by its own thread.
            .choice()
            .when(simple("${exchangeProperty.CamelSplitIndex} > 0"))
            .doTry()
            .unmarshal().bindy(BindyType.Csv, TreeRecord.class)
            .to("seda:process-entry");

        // SEDA: messages are exchanged on a blocking queue,
        //       and consumers are invoked in a separate thread from the producer.
        //       (Staged event-driven architecture)
        from("seda:process-entry?concurrentConsumers=10")
            .bean(AppAggregatorStrategy.class, "setCountyHeader")
            .aggregate(new AppAggregatorStrategy())
            .header("county")
            .completionPredicate(header("CamelSplitComplete").isEqualTo(true))
            .to("direct:print-results");

        from("direct:print-results").process(exchange -> {
            final Map<String, Integer> countMap = (Map<String, Integer>)exchange.getIn().getHeader("currentCount");
            exchange.getIn().setBody(countMap.get("Wicklow"));
        }).to("stream:out");
    }
}
