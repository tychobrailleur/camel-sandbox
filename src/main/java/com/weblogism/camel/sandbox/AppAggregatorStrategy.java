package com.weblogism.camel.sandbox;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;
import org.apache.camel.Message;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AppAggregatorStrategy implements AggregationStrategy {
    public final static Map<String, Integer> count = new ConcurrentHashMap<>();

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        Message msg = newExchange.getIn();
        String header = (String)msg.getHeader("county");
        if (count.containsKey(header)) {
            count.put(header, count.get(header) + 1);
        } else {
            count.put(header, 1);
        }

        msg.setHeader("currentCount", count);
        return newExchange;
    }

    public void setCountyHeader(Exchange exchange, TreeRecord tree) {
        exchange.getMessage().setHeader("county", tree.getCounty());
    }
}
