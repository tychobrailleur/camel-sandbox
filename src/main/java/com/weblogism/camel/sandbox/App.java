package com.weblogism.camel.sandbox;

import org.apache.camel.main.Main;

public class App {

    public static void main(String[] args) throws Exception {
        Main main = new Main(App.class);
        main.run();
    }
}
